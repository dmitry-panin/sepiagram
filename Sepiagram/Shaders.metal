//
//  Shaders.metal
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct OutVertex
{
    float4  position [[position]];
    float2  textureCoordinates [[user(texturecoord)]];
};

#pragma mark - Vertex shaders

vertex OutVertex defaultVertexShader(device float4 * position [[buffer(0)]],
                                     constant packed_float2 * textureCoordinates [[buffer(1)]],
                                     uint vid [[vertex_id]])
{
    OutVertex outVertex;
    outVertex.position = position[vid];
    outVertex.textureCoordinates = textureCoordinates[vid];
    
    return outVertex;
}

#pragma mark - Fragment shaders

fragment half4 defaultTexturedFragmentShader(OutVertex inFragment [[stage_in]],
                                             texture2d<half> tex2D [[texture(0)]])
{
    constexpr sampler quad_sampler;
    half4 color = tex2D.sample(quad_sampler, inFragment.textureCoordinates);
    return color;
}

fragment float4 colorMatrixFragmentShader(OutVertex inFragment [[stage_in]],
                                          constant float4x4 &colorMatrix [[buffer(0)]],
                                          texture2d<float> tex2D [[texture(0)]])
{
    constexpr sampler quad_sampler;
    float4 result = tex2D.sample(quad_sampler, inFragment.textureCoordinates) * colorMatrix;
    return result;
}
