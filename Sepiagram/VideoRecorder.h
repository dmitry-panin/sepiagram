//
//  VideoRecorder.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Foundation;
@import CoreGraphics;
@import AVFoundation;
@import CoreMedia;

typedef NS_ENUM(NSUInteger, VideoRecorderState)
{
    kVideoRecorderStateInitialized,
    kVideoRecorderStateInitializationFailed,
    kVideoRecorderStateRecording,
    kVideoRecorderStateFinalized,
    kVideoRecorderStateRecordingFailed
    
};

@interface VideoRecorder : NSObject

@property(readonly, nonnull)	NSURL*					videoURL;
@property(readonly)             CGSize					videoSize;
@property(readonly)             CMTime					recordedVideoDuration;
@property(readonly)             VideoRecorderState      state;

- (nullable instancetype)initWithVideoURL:(NSURL* _Nonnull)videoURL
                                videoSize:(CGSize)size;

- (void)startRecording;
- (void)finalizeRecordingWithCompletionHandler:(void (^ _Nullable)(void))handler;

- (void)appendPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer presentationTimestamp:(CMTime)pts;
- (void)appendAudioBuffer:(CMSampleBufferRef _Nonnull)sampleBuffer;

@end
