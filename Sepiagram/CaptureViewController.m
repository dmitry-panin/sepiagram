//
//  ViewController.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import AVKit;

#import "CaptureViewController.h"
#import "CameraCapturer.h"
#import "VideoRecorder.h"
#import "PixelBufferDisplayView.h"
#import "SepiaEffect.h"

const CGSize kVideoSize = (CGSize){1080, 1920};

@interface CaptureViewController ()
<CameraCapturerDelegate,
AVPlayerViewControllerDelegate>
{
    BOOL                                _recording;
    
    IBOutlet PixelBufferDisplayView*     _cameraBackgroundView;
    IBOutlet UIButton*                  _recButton;
    IBOutlet UIButton*                  _swapButton;
    IBOutlet UILabel*                   _recDurationLabel;
    IBOutlet UIActivityIndicatorView*   _activityIndicator;
    NSTimer*                            _refreshRecordingDurationTimer;
    
    CameraCapturer*                     _cameraCapturer;
    VideoRecorder*                      _videoRecorder;
    
    id                                  _sepiaEffectLock;
    SepiaEffect*                        _sepiaEffect;
    
    UIInterfaceOrientation              _orientation;
}
@end

@implementation CaptureViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _cameraCapturer = [[CameraCapturer alloc] initWithVideoQueue: nil
                                                      audioQueue: nil];
    
    _sepiaEffectLock = [[NSObject alloc] init];
    
    _cameraCapturer.delegate = self;
    _cameraCapturer.captureOrientation = AVCaptureVideoOrientationPortrait;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(_onApplicationDidResignActiveNotification:)
                                                 name: UIApplicationWillResignActiveNotification
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(_onApplicationDidEnterBackgroundNotification:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [_cameraCapturer start];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    [_cameraCapturer pause];
}

- (IBAction)onRecButtonTap:(id)sender
{
    if(_recording)
    {
        [self _stopRecording];
    }
    else
    {
        [self _startRecording];
    }
}

- (IBAction)onSwapButtonTap:(id)sender
{
    [_cameraCapturer swapCamera];
}

#pragma mark Camera Capturer Delegaate

- (void)cameraCapturer:(CameraCapturer* _Nonnull)sender
didReceiveVideoPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer
 presentationTimestamp:(CMTime)pts
{
    CVPixelBufferRef processedBuffer = nil;
    
    @synchronized (_sepiaEffectLock)
    {
        //lazy loading of the sepia effect based on the Settings
        if(!_sepiaEffect)
        {
            NSNumber* sepiaRendererNumber = [[NSUserDefaults standardUserDefaults] objectForKey: @"SepiaRenderer"];
            SepiaEffectRenderMode renderMode = kSepiaEffectRenderModeMetal;
            
            if(sepiaRendererNumber)
            {
                renderMode = sepiaRendererNumber.integerValue;
            }
            
            _sepiaEffect = [SepiaEffect effectWithRenderMode: renderMode videoSize: kVideoSize];
        }
        
        processedBuffer = [_sepiaEffect retainedProcessedPixelBuffer: pixelBuffer];
    }
    
    if(!processedBuffer)
    {
        processedBuffer = pixelBuffer;
        CFRetain(processedBuffer);
    }
    
    [_cameraBackgroundView displayPixelBuffer: processedBuffer];
    
    if(_videoRecorder)
    {
        [_videoRecorder appendPixelBuffer: processedBuffer presentationTimestamp: pts];
    }
    
    CFRelease(processedBuffer);
}

- (void)cameraCapturer:(CameraCapturer* _Nonnull)sender
didReceiveAudioSampleBuffer:(CMSampleBufferRef _Nonnull)sampleBuffer
{
    if(_videoRecorder)
    {
        [_videoRecorder appendAudioBuffer: sampleBuffer];
    }
}

#pragma mark Private Methods

- (void)_startRecording
{
    [_recButton setImage: [UIImage imageNamed:@"RecordButtonHighlighted"] forState:UIControlStateNormal];
    
    _recDurationLabel.hidden = NO;
    [self _refreshRecordingDurationLabel];
    
    if([[NSFileManager defaultManager] fileExistsAtPath: [self _recordingFileURL].path])
    {
        [[NSFileManager defaultManager] removeItemAtURL: [self _recordingFileURL]
                                                  error: nil];
    }
    
    _videoRecorder = [[VideoRecorder alloc] initWithVideoURL: [self _recordingFileURL]
                                                   videoSize: kVideoSize];
    
    [_videoRecorder startRecording];
    
    _refreshRecordingDurationTimer = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                                      target: self
                                                                    selector: @selector(_onUpdateRecordingDuration)
                                                                    userInfo: nil
                                                                     repeats: YES];
    _recording = YES;
}

- (void)_stopRecording
{
    _recButton.enabled = NO;
    _swapButton.hidden = YES;
    [_activityIndicator startAnimating];
    
    [_refreshRecordingDurationTimer invalidate];
    _refreshRecordingDurationTimer = nil;
    
    [_videoRecorder finalizeRecordingWithCompletionHandler:
     ^{
         _videoRecorder =  nil;
         
         dispatch_async(dispatch_get_main_queue(), ^
            {
                [self _onRecordingFinished];
            });
     }];
}

- (void)_refreshRecordingDurationLabel
{
    int recordingDuration = CMTimeGetSeconds(_videoRecorder.recordedVideoDuration);
    _recDurationLabel.text = [NSString stringWithFormat: @"%.2d:%.2d", recordingDuration / 60, recordingDuration % 60];
}

- (void)_onUpdateRecordingDuration
{
    [self _refreshRecordingDurationLabel];
}

- (void)_onApplicationDidResignActiveNotification:(NSNotification*)notification
{
    if(_recording)
    {
        [self _stopRecording];
    }
}

- (void)_onApplicationDidEnterBackgroundNotification:(NSNotification*)notification
{
    @synchronized (_sepiaEffectLock)
    {
        _sepiaEffect = nil;
    }
}

- (void)_onRecordingFinished
{
    _recButton.enabled = YES;
    _recDurationLabel.hidden = YES;
    _swapButton.hidden = NO;
    [_activityIndicator stopAnimating];
    
    [_recButton setImage: [UIImage imageNamed:@"RecordButton"] forState:UIControlStateNormal];
    _recording = NO;
    
    //creating playback view controller
    AVPlayer* player = [AVPlayer playerWithURL: [self _recordingFileURL]];
    
    AVPlayerViewController* playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.player = player;
    
    [self presentViewController: playerViewController
                       animated: YES
                     completion: ^
     {
         
     }];
}

- (NSURL*)_recordingFileURL
{
    NSString* path = [NSTemporaryDirectory() stringByAppendingPathComponent: @"recording.mp4"];
    return [NSURL fileURLWithPath: path];
}

@end
