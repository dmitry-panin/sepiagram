//
//  SepiaColorMatrix.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SepiaEffect.h"
#import "SepiaEffectMetal.h"
#import "SepiaEffectAccelerate.h"
#import "SepiaEffectCoreImage.h"
#import "SepiaEffectManual.h"

float SepiaColorMatrix[16] = {0.3588, 0.7044, 0.1368, 0.0,
                                0.2990, 0.5870, 0.1140, 0.0,
                                0.2392, 0.4696, 0.0912 ,0.0,
                                0.0, 0.0, 0.0, 1.0};

@implementation SepiaEffect


- (nullable instancetype)initWithVideoSize:(CGSize)size
{
    if(self = [super init])
    {
        
    }
    
    return self;
}
- (nonnull CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer
{
    return nil;
}

+ (SepiaEffect*)effectWithRenderMode:(SepiaEffectRenderMode)renderMode videoSize:(CGSize)size
{
    SepiaEffect* result = nil;
    
    switch (renderMode)
    {
        case kSepiaEffectRenderModeSoftware:
            result = [[SepiaEffectManual alloc] initWithVideoSize: size];
            break;
            
        case kSepiaEffectRenderModeAccelerate:
            result = [[SepiaEffectAccelerate alloc] initWithVideoSize: size];
            break;
            
        case kSepiaEffectRenderModeCoreImage:
            result = [[SepiaEffectCoreImage alloc] initWithVideoSize: size];
            break;
            
        case kSepiaEffectRenderModeMetal:
            result = [[SepiaEffectMetal alloc] initWithVideoSize: size];
            break;
            
        default:
            break;
    }
    
    return result;
}

@end
