//
//  VideoEffect.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Foundation;
@import AVFoundation;

extern float SepiaColorMatrix[16];

typedef NS_ENUM(NSUInteger, SepiaEffectRenderMode)
{
   kSepiaEffectRenderModeSoftware = 0,
    kSepiaEffectRenderModeAccelerate = 1,
    kSepiaEffectRenderModeCoreImage = 2,
    kSepiaEffectRenderModeMetal = 3,
};;

@interface SepiaEffect : NSObject

- (nullable instancetype)initWithVideoSize:(CGSize)size;
- (nonnull CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer;

+ (nullable SepiaEffect*)effectWithRenderMode:(SepiaEffectRenderMode)renderMode videoSize:(CGSize)size;

@end
