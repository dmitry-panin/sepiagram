//
//  SepiaEffectManual.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#import "SepiaEffectManual.h"

@implementation SepiaEffectManual

- (nullable instancetype)initWithVideoSize:(CGSize)size
{
    if(self = [super initWithVideoSize: size])
    {
        
    }
    
    return self;
}

- (CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    if(!pixelBuffer)
    {
        return nil;
    }
    
    //1 Блокируем PixelBuffer
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);

    //2. Получаем доступ к Bitmap
    UInt8* buffer = CVPixelBufferGetBaseAddress(pixelBuffer);
    size_t size = CVPixelBufferGetDataSize(pixelBuffer);

    //3. Применяем эффект
    float input[3] = {0.0f};
    float output[3] = {0.0f};
    
    for(size_t i = 0; i < size / 4; i ++)
    {
        input[0] = buffer[i * 4 + 2];
        input[1] = buffer[i * 4 + 1];
        input[2] = buffer[i * 4 + 0];
        
        output[0] = MIN(input[0] * SepiaColorMatrix[4 * 0 + 0] +
                        input[1] * SepiaColorMatrix[4 * 0 + 1] +
                        input[2] * SepiaColorMatrix[4 * 0 + 2], 255);
        output[1] = MIN(input[0] * SepiaColorMatrix[4 * 1 + 0] +
                        input[1] * SepiaColorMatrix[4 * 1 + 1] +
                        input[2] * SepiaColorMatrix[4 * 1 + 2], 255);
        output[2] = MIN(input[0] * SepiaColorMatrix[4 * 2 + 0] +
                        input[1] * SepiaColorMatrix[4 * 2 + 1] +
                        input[2] * SepiaColorMatrix[4 * 2 + 2], 255);
        

        buffer[i * 4 + 2] = output[0];
        buffer[i * 4 + 1] = output[1];
        buffer[i * 4 + 0] = output[2];
    }

    //4. Разблокируем буфер
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    
    CFRetain(pixelBuffer);
    
    return pixelBuffer;
}

@end
