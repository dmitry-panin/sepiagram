//
//  SepiaEffectMetal.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Metal;

#import "SepiaEffectMetal.h"
#import "MetalDriver.h"

@implementation SepiaEffectMetal
{
    CGSize                          _videoSize;
    
    CVMetalTextureCacheRef          _texturesCache;
    id<MTLRenderPipelineState>      _pipelineState;
    id<MTLBuffer>                   _verticesBuffer;
    id<MTLBuffer>                   _texCoordsBuffer;
    id<MTLBuffer>                   _colorMatrixBuffer;
    
    CVPixelBufferRef                _inputPixelBuffer;
    CVPixelBufferPoolRef            _pixelBufferPool;
}

- (nullable instancetype)initWithVideoSize:(CGSize)size
{
    if(self = [super initWithVideoSize: size])
    {
        _videoSize = size;
        
        [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue: ^
         {
             CVMetalTextureCacheCreate(kCFAllocatorDefault,
                                       NULL,
                                       [MetalDriver sharedDriver].metalDevice,
                                       NULL,
                                       &_texturesCache);
             
             id<MTLFunction> vertexShader = [[[MetalDriver sharedDriver] defaultShadersLibrary] newFunctionWithName:@"defaultVertexShader"];
             id<MTLFunction> fragmentShader = [[[MetalDriver sharedDriver] defaultShadersLibrary] newFunctionWithName:@"colorMatrixFragmentShader"];
             
             MTLRenderPipelineDescriptor *renderPipelineDescriptor = [MTLRenderPipelineDescriptor new];
             renderPipelineDescriptor.vertexFunction = vertexShader;
             renderPipelineDescriptor.fragmentFunction = fragmentShader;
             
             MTLRenderPipelineColorAttachmentDescriptor *pipelineColorAttachmentDescriptor = renderPipelineDescriptor.colorAttachments[0];
             pipelineColorAttachmentDescriptor.pixelFormat = [MetalDriver sharedDriver].defaultMetalPixelFormat;
             
             _pipelineState = [[MetalDriver sharedDriver].metalDevice newRenderPipelineStateWithDescriptor:renderPipelineDescriptor error: nil];
             
             
             _verticesBuffer = [[MetalDriver sharedDriver].metalDevice newBufferWithLength: sizeof(float[16])
                                                                                   options: MTLResourceCPUCacheModeDefaultCache];
             [MetalDriver fullFrameSprite: (float*)_verticesBuffer.contents];
             
             _texCoordsBuffer = [[MetalDriver sharedDriver].metalDevice newBufferWithLength: sizeof(float[8])
                                                                                    options:MTLResourceCPUCacheModeDefaultCache];
             
             [MetalDriver textureCoordinatesForFullTexture: (float*)_texCoordsBuffer.contents verticallyFlipped: YES];
             
             _colorMatrixBuffer = [[MetalDriver sharedDriver].metalDevice newBufferWithLength: sizeof(float[16])
                                                                                    options:MTLResourceCPUCacheModeDefaultCache];
             
             memcpy(_colorMatrixBuffer.contents, SepiaColorMatrix, sizeof(SepiaColorMatrix));
             
             
             NSDictionary *pixelBufferAttributes = @{(NSString *)kCVPixelBufferIOSurfacePropertiesKey:@{},
                                                     (NSString *)kCVPixelBufferWidthKey:@(_videoSize.width),
                                                     (NSString *)kCVPixelBufferHeightKey:@(_videoSize.height),
                                                     (NSString *)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32BGRA)};
             
            CVPixelBufferPoolCreate(kCFAllocatorDefault,
                                              NULL,
                                              (__bridge CFDictionaryRef _Nullable)(pixelBufferAttributes),
                                              &_pixelBufferPool);
             
         }];
    }
    
    return self;
}

- (CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    __block CVPixelBufferRef outputPixelBuffer = 0;
    
    [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue: ^
     {
         //retaining pixel buffer
         if(_inputPixelBuffer)
         {
             CFRelease(_inputPixelBuffer);
             _inputPixelBuffer = NULL;
         }
         
         _inputPixelBuffer = pixelBuffer;
         
         if(!_inputPixelBuffer)
         {
             return ;
         }
         
         CFRetain(_inputPixelBuffer);
         
         CVMetalTextureRef texture = NULL;
         
         //loading pixel buffer to texture
         CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                   _texturesCache,
                                                   _inputPixelBuffer,
                                                   NULL,
                                                   [MetalDriver sharedDriver].defaultMetalPixelFormat,
                                                   CVPixelBufferGetWidth(_inputPixelBuffer),
                                                   CVPixelBufferGetHeight(_inputPixelBuffer),
                                                   0,
                                                   &texture);
         
         if(!texture)
         {
             return;
         }
         
         //loading render target pixel buffer
         CVPixelBufferPoolCreatePixelBuffer(NULL,
                                            _pixelBufferPool,
                                            &outputPixelBuffer);
         
         if(!outputPixelBuffer)
         {
             return;
         }
         
         CVMetalTextureRef outputTexture = NULL;
         
         //loading pixel buffer to texture
         CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                   _texturesCache,
                                                   outputPixelBuffer,
                                                   NULL,
                                                   [MetalDriver sharedDriver].defaultMetalPixelFormat,
                                                   CVPixelBufferGetWidth(outputPixelBuffer),
                                                   CVPixelBufferGetHeight(outputPixelBuffer),
                                                   0,
                                                   &outputTexture);
         
         if(!outputTexture)
         {
             return;
         }
         
         MTLRenderPassDescriptor *renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
         renderPassDescriptor.colorAttachments[0].texture = CVMetalTextureGetTexture(outputTexture);
         renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, 0.0);
         renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
         renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
         
         id<MTLCommandBuffer> commandBuffer = [[MetalDriver sharedDriver].defaultCommandQueue commandBuffer];
         
         id<MTLRenderCommandEncoder> renderCommandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
         
         [renderCommandEncoder setRenderPipelineState: _pipelineState];
         
         [renderCommandEncoder setVertexBuffer: _verticesBuffer
                                        offset: 0
                                       atIndex: 0];
         
         [renderCommandEncoder setVertexBuffer: _texCoordsBuffer
                                        offset: 0
                                       atIndex: 1];
         
         [renderCommandEncoder setFragmentBuffer: _colorMatrixBuffer
                                        offset: 0
                                       atIndex: 0];
         
         [renderCommandEncoder setFragmentTexture: CVMetalTextureGetTexture(texture)
                                          atIndex: 0];
         
         [renderCommandEncoder drawPrimitives: MTLPrimitiveTypeTriangleStrip
                                  vertexStart: 0
                                  vertexCount: 4
                                instanceCount: 1];
         
         [renderCommandEncoder endEncoding];
         
         [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> _Nonnull commandBuffer)
          {
              CFRelease(texture);
              CFRelease(outputTexture);
          }];
         
         [commandBuffer commit];
         [commandBuffer waitUntilCompleted];
         
     }];
    
    return outputPixelBuffer;
}

- (void)dealloc
{
    [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue:^
     {
         if (_texturesCache)
         {
             CVMetalTextureCacheFlush(_texturesCache, 0);
             CFRelease(_texturesCache);
         }
         
         if (_pixelBufferPool)
         {
             CVPixelBufferPoolRelease(_pixelBufferPool);
         }
     }];
}

@end
