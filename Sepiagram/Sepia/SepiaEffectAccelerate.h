//
//  SepiaEffectAccelerate.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/29/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Foundation;

#import "SepiaEffect.h"

@interface SepiaEffectAccelerate : SepiaEffect

@end
