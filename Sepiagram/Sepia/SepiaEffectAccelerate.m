//
//  SepiaEffectAccelerate.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/29/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Accelerate;

#import "SepiaEffectAccelerate.h"

@interface SepiaEffectAccelerate()
{
    int16_t sepiaMatrix[16];
    int32_t sepiaMatrixDevisor;
}

@end

@implementation SepiaEffectAccelerate

- (nullable instancetype)initWithVideoSize:(CGSize)size
{
    if(self = [super initWithVideoSize: size])
    {
        sepiaMatrixDevisor = 256;
        
        int16_t t = 0;
        
        // our original matrix is build for case
        //    [x x x x]  [R
        //    [x x x x]   G
        //    [x x x x]   B
        //    [x x x x]   A]
        
        // we need to be converting it for the format
        //    [x x x x]
        //    [x x x x]  [B G R A]
        //    [x x x x]
        //    [x x x x]
        
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                sepiaMatrix[i * 4 + j] = SepiaColorMatrix[j * 4 + i] * sepiaMatrixDevisor;
            }
            
            t = sepiaMatrix[i * 4 + 2];
            sepiaMatrix[i * 4 + 2] = sepiaMatrix[i * 4 + 0];
            sepiaMatrix[i * 4 + 0] = t;
        }
    }
    return self;
}

- (CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer
{
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    vImage_Buffer inputBuffer;
    inputBuffer.data = CVPixelBufferGetBaseAddress(pixelBuffer);
    inputBuffer.rowBytes = CVPixelBufferGetBytesPerRow(pixelBuffer);
    inputBuffer.width = CVPixelBufferGetWidth(pixelBuffer);
    inputBuffer.height = CVPixelBufferGetHeight(pixelBuffer);
    
    vImageMatrixMultiply_ARGB8888(&inputBuffer, &inputBuffer, sepiaMatrix, sepiaMatrixDevisor, 0, 0, 0);
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    
    CFRetain(pixelBuffer);
    
    return pixelBuffer;
}

@end
