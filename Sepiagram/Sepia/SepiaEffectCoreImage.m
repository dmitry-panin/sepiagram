//
//  SepiaEffectCoreImage.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import CoreImage;
#import "SepiaEffectCoreImage.h"

@implementation SepiaEffectCoreImage
{
    CIContext*  _context;
    CIFilter*   _sepiaFilter;
}

- (nullable instancetype)initWithVideoSize:(CGSize)size
{
    if(self = [super initWithVideoSize:size])
    {
        _context = [CIContext contextWithOptions: nil];
        _sepiaFilter = [CIFilter filterWithName: @"CISepiaTone"];
    }
    
    return self;
}

- (CVPixelBufferRef)retainedProcessedPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    CIImage* input = [CIImage imageWithCVImageBuffer: pixelBuffer];
    [_sepiaFilter setValue:input forKey: kCIInputImageKey];
    
    CIImage* outputImage = _sepiaFilter.outputImage;
    [_context render:outputImage toCVPixelBuffer:  pixelBuffer];
    
    CFRetain(pixelBuffer);
    
    return pixelBuffer;
}

@end
