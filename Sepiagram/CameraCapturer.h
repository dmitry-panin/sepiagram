//
//  CameraCapturer.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Foundation;
@import AVFoundation;
@import CoreMedia;

@class CameraCapturer;

@protocol CameraCapturerDelegate <NSObject>

@optional
- (void)cameraCapturer:(CameraCapturer* _Nonnull)sender didReceiveVideoPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer presentationTimestamp:(CMTime)pts;
- (void)cameraCapturer:(CameraCapturer* _Nonnull)sender didReceiveAudioSampleBuffer:(CMSampleBufferRef _Nonnull)sampleBuffer;

@end

@interface CameraCapturer : NSObject

@property(weak, nullable) id<CameraCapturerDelegate>                    delegate;
@property(readonly, getter=isStarted)       BOOL                        started;
@property(assign)       AVCaptureVideoOrientation                       captureOrientation;
@property(assign)       BOOL                                            mirrored;

- (nullable instancetype)initWithVideoQueue:(nullable dispatch_queue_t)videoQueue
                                 audioQueue:(nullable dispatch_queue_t)audioQeeue;

- (void)start;
- (void)pause;
- (void)swapCamera;

@end
