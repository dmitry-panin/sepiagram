//
//  MetalDriver.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Metal;
@import Foundation;

@interface MetalDriver : NSObject

@property (strong, nonatomic, readonly)     id<MTLDevice>                               metalDevice;
@property (strong, nonatomic, readonly)     id<MTLLibrary>                              defaultShadersLibrary;
@property (strong, nonatomic, readonly)     id<MTLCommandQueue>                         defaultCommandQueue;
@property (readonly)                        MTLPixelFormat                              defaultMetalPixelFormat;
@property (readonly)                        dispatch_queue_t                            videoProcessingQueue;

+ (instancetype)sharedDriver;

//running block synchronously on processing queue. If called from
//processing queue just performing the block
- (void)runSynchronouslyOnVideoProcessingQueue:(void(^)())block;

@end

@interface MetalDriver(HelperFunctions)

+ (void)fullFrameSprite:(float[16])buffer;
+ (void)textureCoordinatesForFullTexture:(float[8])buffer verticallyFlipped:(BOOL)flipped;

@end

@interface MetalDriver(Initialization)

//making sure object can't be instantiated in another way
+ (instancetype)alloc                                   __attribute__((unavailable("alloc not available, call sharedDriver instead")));
- (instancetype)init                                    __attribute__((unavailable("init not available, call sharedDriver instead")));
+ (instancetype)new                                     __attribute__((unavailable("new not available, call sharedDriver instead")));
+ (instancetype)allocWithZone:(struct _NSZone *)zone    __attribute__((unavailable("allocWithZone not available, call sharedDriver instead")));
- (instancetype)copyWithZone:(NSZone *)zone             __attribute__((unavailable("copyWithZone not available, call sharedDriver instead")));
- (instancetype)copy                                    __attribute__((unavailable("copy not available, call sharedDriver instead")));
- (instancetype)mutableCopy                             __attribute__((unavailable("mutableCopy not available, call sharedDriver instead")));
- (instancetype)mutableCopyWithZone                     __attribute__((unavailable("mutableCopyWithZone not available, call sharedDriver instead")));

@end

