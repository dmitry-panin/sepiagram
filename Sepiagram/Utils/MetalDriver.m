//
//  MetalDriver.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#import "MetalDriver.h"

static MetalDriver* kStaticDriver = nil;
static const char*  kStaticVideoProcessingQueueKey = "com.metaldriver.videoprocessingqueuekey";

@interface MetalDriver()
{
    
}
@end


@implementation MetalDriver

#pragma mark - Init/Dealloc

- (instancetype)initUniqueInstance
{
    if (self = [super init])
    {
        _metalDevice = MTLCreateSystemDefaultDevice();
        _defaultShadersLibrary = [_metalDevice newDefaultLibrary];
        _defaultCommandQueue = [_metalDevice newCommandQueue];
        _defaultMetalPixelFormat = MTLPixelFormatBGRA8Unorm;
        
        if (!_metalDevice)
        {
            NSAssert(0, @"Metal is not supported by this hardware");
            return nil;
        }
        
        _videoProcessingQueue = dispatch_queue_create(kStaticVideoProcessingQueueKey, NULL);
       
        //attaching key to the video processing queue. Will use it to distinguish whether code is already running on video processing queue
        dispatch_queue_set_specific(_videoProcessingQueue, kStaticVideoProcessingQueueKey, (__bridge void *)self, NULL);
    }
    
    return self;
}

+ (instancetype)sharedDriver
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken,
                  ^{
                      kStaticDriver = [[super alloc] initUniqueInstance];
                  });
    
    return kStaticDriver;
}

- (void)runSynchronouslyOnVideoProcessingQueue:(void(^)())block
{
    @autoreleasepool
    {
        //checking wheter code is already running on the video processing queue
        if (dispatch_get_specific(kStaticVideoProcessingQueueKey) == (__bridge void *)self)
        {
            //code is already in the video processing queue. Just performing block
            block();
        }
        else
        {
            //dispatching code to the video processing queue
            dispatch_sync(self.videoProcessingQueue, block);
        }
    }
}

+ (void)fullFrameSprite:(float[16])buffer
{
    //bottom left
    buffer[0] = -1.0;
    buffer[1] = -1.0;
    buffer[2] = 0.0f;
    buffer[3] = 1.0f;
    
    //bottom right
    buffer[4] = 1.0;
    buffer[5] = -1.0;
    buffer[6] = 0.0f;
    buffer[7] = 1.0f;
    
    //top left
    buffer[8] = -1.0f;
    buffer[9] = 1.0f;
    buffer[10] = 0.0f;
    buffer[11] = 1.0f;
    
    //top right
    buffer[12] = 1.0;
    buffer[13] = 1.0f;
    buffer[14] = 0.0f;
    buffer[15] = 1.0f;
}

+ (void)textureCoordinatesForFullTexture:(float[8])buffer verticallyFlipped:(BOOL)flipped
{
    float minY = 0.0f;
    float maxY = 1.0f;
    
    if(flipped)
    {
        minY = 1.0f;
        maxY = 0.0f;
    }
    
    //bottom left
    buffer[0] = 0.0f;
    buffer[1] = minY;
    
    //bottom right
    buffer[2] = 1.0f;
    buffer[3] = minY;
    
    //top left
    buffer[4] = 0.0f;
    buffer[5] = maxY;
    
    //top right
    buffer[6] = 1.0f;
    buffer[7] = maxY;
}

@end
