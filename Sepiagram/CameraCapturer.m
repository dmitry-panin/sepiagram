//
//  CameraCapturer.m
//  OldProjector
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CoreMedia.h>
#import "CameraCapturer.h"

@interface CameraCapturer()
<AVCaptureVideoDataOutputSampleBufferDelegate,
AVCaptureAudioDataOutputSampleBufferDelegate>
{
    AVCaptureSession*					_captureSession;
    
    dispatch_queue_t					_audioReceivingQueue;
    dispatch_queue_t                    _videoFramesReceivingQueue;
    
    AVCaptureDeviceInput*				_frontalCaptureInput;
    AVCaptureDeviceInput*				_backCaptureInput;
    
    AVCaptureDeviceInput*				_currentCaptureInput;
    
    AVCaptureDeviceInput*				_audioInput;
    
    AVCaptureAudioDataOutput*			_audioOutput;
    AVCaptureVideoDataOutput*			_videoStreamOutput;
    
    AVCaptureVideoOrientation			_captureOrientation;
    BOOL                                _mirrored;
}

@end

@implementation CameraCapturer

- (nullable instancetype)initWithVideoQueue:(nullable dispatch_queue_t)videoQueue
                                 audioQueue:(nullable dispatch_queue_t)audioQeeue
{
    if(self = [super init])
    {
        _videoFramesReceivingQueue = videoQueue;
        _audioReceivingQueue = audioQeeue;
        
        if(![self _initCapture])
        {
            return nil;
        }
    }
    
    return self;
}

- (void)dealloc
{
    [self pause];
    [self _releaseCapture];
}

- (void)start
{
    [_captureSession startRunning];
}

- (void)pause
{
    [_captureSession stopRunning];
}

- (void)swapCamera
{
    [_captureSession beginConfiguration];
    
    [_captureSession removeInput: _currentCaptureInput];
    
    if(_currentCaptureInput == _frontalCaptureInput)
    {
        _currentCaptureInput = _backCaptureInput;
        _mirrored = NO;
    }
    else
    {
        _currentCaptureInput = _frontalCaptureInput;
        _mirrored = YES;
    }
    
    [_captureSession addInput: _currentCaptureInput];
    
    AVCaptureConnection *videoConnection = NULL;
    for ( AVCaptureConnection *connection in [_videoStreamOutput connections])
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
            }
        }
    }
    
    if([videoConnection isVideoOrientationSupported])
    {
        [videoConnection setVideoOrientation: _captureOrientation];
    }
    
    if([videoConnection isVideoMirroringSupported])
    {
        [videoConnection setVideoMirrored: _mirrored];
    }
    
    [_captureSession commitConfiguration];
}

- (BOOL)isStarted
{
    return [_captureSession isRunning];
}


- (AVCaptureVideoOrientation)captureOrientation
{
    return _captureOrientation;
}

- (void)setCaptureOrientation:(AVCaptureVideoOrientation)captureOrientation
{
    //configure video
    [_captureSession beginConfiguration];
    
    AVCaptureConnection *videoConnection = NULL;
    for ( AVCaptureConnection *connection in [_videoStreamOutput connections])
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
            }
        }
        
    }
    if([videoConnection isVideoOrientationSupported])
    {
        _captureOrientation = captureOrientation;
        [videoConnection setVideoOrientation: _captureOrientation];
    }
    
    [_captureSession commitConfiguration];
}

- (BOOL)mirrored
{
    return _mirrored;
}

- (void)setMirrored:(BOOL)mirrored
{
    _mirrored = mirrored;
    
    //configure video
    [_captureSession beginConfiguration];
    
    AVCaptureConnection *videoConnection = NULL;
    for ( AVCaptureConnection *connection in [_videoStreamOutput connections])
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
            }
        }
        
    }
    
    if([videoConnection isVideoMirroringSupported])
    {
        [videoConnection setVideoMirrored: _mirrored];
    }
    
    [_captureSession commitConfiguration];
}

#pragma mark - AVCaptureSession Delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    CMTime pts = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
    
    id<CameraCapturerDelegate> strongDelegate = self.delegate;
    
    if([captureOutput isEqual: _audioOutput])
    {
        if([strongDelegate respondsToSelector: @selector(cameraCapturer:didReceiveAudioSampleBuffer:)])
        {
            [strongDelegate cameraCapturer:self didReceiveAudioSampleBuffer: sampleBuffer];
        }
    }
    else
    {
        if([strongDelegate respondsToSelector: @selector(cameraCapturer:didReceiveVideoPixelBuffer:presentationTimestamp:)])
        {
            CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
            [strongDelegate cameraCapturer:self didReceiveVideoPixelBuffer:pixelBuffer presentationTimestamp: pts];
        }
    }
}

#pragma mark - Private

- (BOOL)_initCapture
{
    AVCaptureDevice* deviceWithFrontCamera = [self _deviceForCameraWithPosition: AVCaptureDevicePositionFront];
    AVCaptureDevice* deviceWithBackCamera = [self _deviceForCameraWithPosition: AVCaptureDevicePositionBack];
    
    _frontalCaptureInput = [[AVCaptureDeviceInput alloc] initWithDevice: deviceWithFrontCamera error:nil];
    _backCaptureInput = [[AVCaptureDeviceInput alloc] initWithDevice: deviceWithBackCamera error:nil];
    
    _currentCaptureInput = _frontalCaptureInput;
    
    //setup the output
    _videoStreamOutput = [[AVCaptureVideoDataOutput alloc] init];
    _videoStreamOutput.alwaysDiscardsLateVideoFrames = YES;
    
    if(!_videoFramesReceivingQueue)
    {
        _videoFramesReceivingQueue = dispatch_queue_create("com.oldprojector.framesReceivingQueue", 0);
    }
    
    [_videoStreamOutput setSampleBufferDelegate:self queue: _videoFramesReceivingQueue];
    
    // Set video output format to BGRA
    NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
    NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    
    NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key];
    [_videoStreamOutput setVideoSettings:videoSettings];
    
    
    AVCaptureDevice* audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    _audioInput = [AVCaptureDeviceInput deviceInputWithDevice: audioCaptureDevice
                                                        error: nil];
    
    _audioOutput = [[AVCaptureAudioDataOutput alloc] init];
    
    if(!_audioReceivingQueue)
    {
        _audioReceivingQueue = dispatch_queue_create("com.touchcast.audioReceivingQueue", 0);
    }
    
    [_audioOutput setSampleBufferDelegate:self queue: _audioReceivingQueue];
    
    // And we create a capture session
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.usesApplicationAudioSession = YES;
    _captureSession.automaticallyConfiguresApplicationAudioSession = YES;
    
    //adding input and output
    if ([_captureSession canAddInput: _frontalCaptureInput])
    {
        [_captureSession addInput: _frontalCaptureInput];
    }
    else
    {
        return NO;
    }
    
    if([_captureSession canAddInput: _audioInput])
    {
        [_captureSession addInput: _audioInput];
    }
    else
    {
        return NO;
    }
    
     _captureSession.sessionPreset = AVCaptureSessionPresetHigh;
    
    
    if ([_captureSession canAddOutput:_videoStreamOutput])
    {
        [_captureSession addOutput:_videoStreamOutput];
    }
    else
    {
        return NO;
    }
    
    if ([_captureSession canAddOutput:_audioOutput])
    {
        [_captureSession addOutput:_audioOutput];
    }
    else
    {
        return NO;
    }
    
    //configure video
    [_captureSession beginConfiguration];
    
    AVCaptureConnection *videoConnection = NULL;
    for ( AVCaptureConnection *connection in [_videoStreamOutput connections])
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
            }
        }
    }
    
    _captureOrientation = AVCaptureVideoOrientationPortrait;
    
    if([videoConnection isVideoOrientationSupported])
    {
        [videoConnection setVideoOrientation: _captureOrientation];
    }
    
    if (videoConnection.supportsVideoStabilization)
    {
        videoConnection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationModeStandard;
    }
    
    [_captureSession commitConfiguration];
    
    if(_currentCaptureInput == _frontalCaptureInput)
    {
        self.mirrored = YES;
    }
    else
    {
        self.mirrored = NO;
    }
    
    return YES;
    
}

- (void)_releaseCapture
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
    [_captureSession stopRunning];
    
    if(_frontalCaptureInput)
    {
        [_captureSession removeInput:_frontalCaptureInput];
    }
    
    if(_backCaptureInput)
    {
        [_captureSession removeInput:_backCaptureInput];
    }
    
    if(_audioInput)
    {
        [_captureSession removeInput:_audioInput];
    }
    
    [_captureSession removeOutput:_audioOutput];
    
    _audioInput = nil;
    _audioOutput = nil;
    
    [_captureSession removeOutput:_videoStreamOutput];
    _frontalCaptureInput = nil;
    _videoStreamOutput = nil;
    
    if(_backCaptureInput)
    {
        _backCaptureInput = nil;
    }
    
    _captureSession = nil;
}

- (AVCaptureDevice *)_deviceForCameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            return device;
        }
    }
    return nil;
}


@end

