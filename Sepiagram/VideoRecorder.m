//
//  VideoRecorder.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

#import "VideoRecorder.h"

@interface VideoRecorder()
{
    AVAssetWriter*							_fileWriter;
    AVAssetWriterInput*						_writerVideoInput;
    AVAssetWriterInput*						_writerAudioInput;
    AVAssetWriterInputPixelBufferAdaptor*	_adaptor;
    
    NSRecursiveLock*						_audioProcessingLock;
    NSRecursiveLock*						_videoProcessingLock;
    BOOL									_preparingForStopping;
    
    BOOL                                    _recordingSessionStarted;
}
@end

@implementation VideoRecorder

- (nullable instancetype)initWithVideoURL:(NSURL* _Nonnull)videoURL
                                videoSize:(CGSize)size
{
    NSParameterAssert(videoURL);
    
    if ((self = [super init]))
    {
        _videoURL = videoURL;
        _videoSize = size;
        _recordedVideoDuration = kCMTimeZero;
        
        [self _initAssetWriter];
    }
    
    return self;
}

- (void)startRecording
{
    if (_state != kVideoRecorderStateInitialized)
    {
        return;
    }
    
    [_fileWriter startWriting];
    [self _setRecorderState: kVideoRecorderStateRecording];
}


- (void)finalizeRecordingWithCompletionHandler:(void (^)(void))handler
{
    if (_state != kVideoRecorderStateRecording)
    {
        NSLog(@"WARNING: Recording is already finalized. Skipping");
        return;
    }
    
    if(_preparingForStopping)
    {
        return;
    }
    
    [_audioProcessingLock lock];
    [_videoProcessingLock lock];
    
    if(_fileWriter.status == AVAssetWriterStatusWriting)
    {
        [_writerVideoInput markAsFinished];
        [_writerAudioInput markAsFinished];
    }
    
    _preparingForStopping = YES;
    
    [_videoProcessingLock unlock];
    [_audioProcessingLock unlock];
    
    [_fileWriter finishWritingWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^
            {
                [self _clearWriters];
                [self _setRecorderState: kVideoRecorderStateFinalized];
                
                if(handler)
                {
                    handler();
                }
            });
     }];
}

- (void)appendPixelBuffer:(CVPixelBufferRef)pixelBuffer presentationTimestamp:(CMTime)pts
{
    [_videoProcessingLock lock];
    
    @try
    {
        if (_preparingForStopping)
        {
            return;
        }
        
        if (_state != kVideoRecorderStateRecording)
        {
            return;
        }
        
        if (_fileWriter.status != AVAssetWriterStatusWriting)
        {
            [self performSelectorOnMainThread: @selector(_invokeAbnormalTerminationDelegateMethod)
                                   withObject: nil
                                waitUntilDone: NO];
            
            return;
        }
        
        @autoreleasepool
        {
            if ([_writerVideoInput isReadyForMoreMediaData])
            {
                if (pixelBuffer && !_preparingForStopping)
                {
                    if(!_recordingSessionStarted)
                    {
                        [_fileWriter startSessionAtSourceTime: pts];
                        _recordingSessionStarted = YES;
                    }
                    
                    if(![_adaptor appendPixelBuffer:pixelBuffer withPresentationTime: pts])
                    {
                        NSLog(@"VIDEO RECORDER: Failed to append image buffer at time %f with error %@",
                                     CMTimeGetSeconds(self.recordedVideoDuration),
                                     [_fileWriter error]);
                        
                         dispatch_async(dispatch_get_main_queue(), ^
                                        {
                                            [self _invokeAbnormalTerminationDelegateMethod];
                                        });

                        return;
                    }
                }
            }
            else
            {
                NSLog(@"VIDEO RECORDER: Video recorder is busy. Video frame will be ignored");
            }
        }
    }
    @finally
    {
        [_videoProcessingLock unlock];
    }
}

-(void)appendAudioBuffer:(CMSampleBufferRef)sampleBuffer
{
    [_audioProcessingLock lock];
   
    @try
    {
        if (_preparingForStopping)
        {
            return;
        }
        
        if (_state != kVideoRecorderStateRecording)
        {
            return;
        }
        
        if(!_recordingSessionStarted)
        {
            return;
        }
        
        if (_fileWriter.status != AVAssetWriterStatusWriting)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
               [self _invokeAbnormalTerminationDelegateMethod];
            });
            
            return;
        }
        
        @autoreleasepool
        {
            if ([_writerAudioInput isReadyForMoreMediaData])
            {
                if ([_writerAudioInput appendSampleBuffer: sampleBuffer])
                {
                    _recordedVideoDuration = CMTimeAdd(_recordedVideoDuration, CMSampleBufferGetDuration(sampleBuffer));
                }
                else
                {
                    [_audioProcessingLock unlock];
                    
                    dispatch_async(dispatch_get_main_queue(), ^
                    {
                       [self _invokeAbnormalTerminationDelegateMethod];
                    });
                
                    return;
                }
            }
            else
            {
                NSLog(@"VIDEO RECORDER: Video recorder is busy. Audio data will be ignored");
            }
        }
    }
    @finally
    {
        [_audioProcessingLock unlock];
    }
}


#pragma mark -
#pragma mark Private methods implementation

- (NSDictionary*)_videoEncoderParams
{
    long bitsPerSecond = 2000 * 1024;
    
    NSDictionary *codecSettings = @{
                                    AVVideoAverageBitRateKey            :   @(bitsPerSecond),
                                    AVVideoProfileLevelKey              :   AVVideoProfileLevelH264HighAutoLevel,
                                    AVVideoExpectedSourceFrameRateKey   :   @(30)
                                    };
    
    NSDictionary* result = @{
                             AVVideoCodecKey                    :   AVVideoCodecH264,
                             AVVideoWidthKey                    :   @(_videoSize.width),
                             AVVideoHeightKey                   :   @(_videoSize.height),
                             AVVideoCompressionPropertiesKey    :   codecSettings
                             };
    
    return result;
}

- (NSDictionary*)_audioEncoderParams
{
    AudioChannelLayout acl;
    bzero( &acl, sizeof(acl));
    acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
    
    return @{
             AVFormatIDKey          :   @(kAudioFormatMPEG4AAC),
             AVNumberOfChannelsKey  :   @(1),
             AVSampleRateKey        :   @(44100.F),
             AVEncoderBitRateKey    :   @(128000),
             AVChannelLayoutKey     :   [NSData dataWithBytes:&acl length:sizeof(acl)]
             };
}

-(void)_initAssetWriter
{
    NSError* error = nil;
    
    _fileWriter = [[AVAssetWriter alloc] initWithURL: _videoURL
                                            fileType: AVFileTypeMPEG4
                                               error: &error];
    
    if(error)
    {
        [self _setRecorderState: kVideoRecorderStateInitializationFailed];
        return;
    }
    
    _fileWriter.shouldOptimizeForNetworkUse = YES;
    
    _writerVideoInput = [AVAssetWriterInput assetWriterInputWithMediaType: AVMediaTypeVideo
                                                           outputSettings: [self _videoEncoderParams]];
    
    float defaultTimeScale = 1.0f / 30.0f;
    [_writerVideoInput setMediaTimeScale: defaultTimeScale];
    
    _writerAudioInput = [AVAssetWriterInput assetWriterInputWithMediaType: AVMediaTypeAudio
                                                           outputSettings: [self _audioEncoderParams]];
    
    _writerAudioInput.expectsMediaDataInRealTime = YES;
    
    //creating pixel buffer adaptor
    NSMutableDictionary *inputPixelAttributes = [[NSMutableDictionary alloc] init];
    
    [inputPixelAttributes setObject: [NSNumber numberWithUnsignedInt: kCVPixelFormatType_32BGRA]
                             forKey: (NSString*)kCVPixelBufferPixelFormatTypeKey];
    
    [inputPixelAttributes setObject: [NSNumber numberWithInt: _videoSize.width]
                             forKey:(NSString*)kCVPixelBufferWidthKey];
    
    [inputPixelAttributes setObject:[NSNumber numberWithInt: _videoSize.height]
                             forKey:(NSString*)kCVPixelBufferHeightKey];
    
    _adaptor = [AVAssetWriterInputPixelBufferAdaptor
                assetWriterInputPixelBufferAdaptorWithAssetWriterInput:_writerVideoInput
                sourcePixelBufferAttributes: inputPixelAttributes];
    
    
    NSParameterAssert(_writerVideoInput);
    NSParameterAssert([_fileWriter canAddInput:_writerVideoInput]);
    
    _writerVideoInput.expectsMediaDataInRealTime = YES;
    [_fileWriter addInput:_writerVideoInput];
    [_fileWriter addInput:_writerAudioInput];
    
    _videoProcessingLock = [[NSRecursiveLock alloc] init];
    _audioProcessingLock = [[NSRecursiveLock alloc] init];
}

-(void)_clearWriters
{
    if (_adaptor)
    {
        _adaptor = nil;
    }
    
    if (_writerVideoInput)
    {
        _writerVideoInput = nil;
    }
    
    if (_writerAudioInput)
    {
        _writerAudioInput = nil;
    }
    
    if (_fileWriter)
    {
        _fileWriter = nil;
    }
    
    [_audioProcessingLock lock];
    [_audioProcessingLock unlock];
    _audioProcessingLock = nil;
    
    [_videoProcessingLock lock];
    [_videoProcessingLock unlock];
    _videoProcessingLock = nil;
}

- (void)_setRecorderState:(VideoRecorderState)state
{
    if(_state == state)
    {
        return;
    }
    
    [self willChangeValueForKey: @"state"];
    _state = state;
    [self didChangeValueForKey: @"state"];
}

- (void)_invokeAbnormalTerminationDelegateMethod
{
    if(_preparingForStopping)
    {
        return;
    }
    
    if(_state != kVideoRecorderStateRecording)
    {
        return;
    }
    
    if(_preparingForStopping)
    {
        return;
    }
    
    NSLog(@"VIDEO RECORDER: Stopping the recording abnormally");
    
    _preparingForStopping = YES;
    
    if(_fileWriter.status == AVAssetWriterStatusWriting)
    {
        [_writerVideoInput markAsFinished];
        [_writerAudioInput markAsFinished];
    }
    
    __weak VideoRecorder* weakSelf = self;
    
    if (_fileWriter.status == AVAssetWriterStatusWriting)
    {
        [_fileWriter finishWritingWithCompletionHandler: ^
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                VideoRecorder* strongSelf = weakSelf;
                                
                                [strongSelf _clearWriters];
                                [strongSelf _setRecorderState: kVideoRecorderStateRecordingFailed];
                            });
         }];
    }
    else
    {
        [self _clearWriters];
        [self _setRecorderState: kVideoRecorderStateRecordingFailed];
    }
}


@end
