//
//  PixelBufferDisplayView.h
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Foundation;
@import UIKit;

@interface PixelBufferDisplayView : UIView

- (nonnull instancetype)initWithFrame:(CGRect)frame;

- (void)displayPixelBuffer:(CVPixelBufferRef _Nonnull)pixelBuffer;

@end
