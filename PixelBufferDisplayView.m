//
//  PixelBufferDisplayView.m
//  Sepiagram
//
//  Created by Dmitry Panin on 8/28/16.
//  Copyright © 2016 Dmitry Panin. All rights reserved.
//

@import Metal;

#import "PixelBufferDisplayView.h"
#import "MetalDriver.h"

@interface PixelBufferDisplayView()
{
    CVMetalTextureCacheRef          _texturesCache;
    id<MTLRenderPipelineState>      _pipelineState;
    id<MTLBuffer>                   _verticesBuffer;
    id<MTLBuffer>                   _texCoordsBuffer;
    
    CAMetalLayer*                   _metalLayer;
    
    CVPixelBufferRef                _pixelBuffer;
}

@end

@implementation PixelBufferDisplayView

+ (Class)layerClass
{
    return [CAMetalLayer class];
}

- (  instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame: frame])
    {
        [self _setupMetal];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self _setupMetal];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)dealloc
{
    [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue: ^
     {
         if(_texturesCache)
         {
             CFRelease(_texturesCache);
         }
         
         if(_pixelBuffer)
         {
             CFRelease(_texturesCache);
         }
         
     }];
}

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer
{
    if(_metalLayer.bounds.size.width == 0 || _metalLayer.bounds.size.height == 0)
    {
        return;
    }
    
    [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue: ^
     {
         //retaining pixel buffer
         if(_pixelBuffer)
         {
             CFRelease(_pixelBuffer);
             _pixelBuffer = NULL;
         }
         
         _pixelBuffer = pixelBuffer;
         
         if(!_pixelBuffer)
         {
             return ;
         }
         
         CFRetain(_pixelBuffer);
         
         CVMetalTextureRef texture = NULL;
         
         //loading pixel buffer to texture
         CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                _texturesCache,
                                                _pixelBuffer,
                                                NULL,
                                                [MetalDriver sharedDriver].defaultMetalPixelFormat,
                                                CVPixelBufferGetWidth(_pixelBuffer),
                                                CVPixelBufferGetHeight(_pixelBuffer),
                                                0,
                                                &texture);
         
         if(!texture)
         {
             return;
         }
         
         //drawing texture
         id<CAMetalDrawable> drawable = nil;
         
         drawable = [_metalLayer nextDrawable];
         
         if (!drawable)
         {
             return;
         }
         
         id<MTLTexture> frameBufferTexture = drawable.texture;
         
         MTLRenderPassDescriptor *renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
         renderPassDescriptor.colorAttachments[0].texture = frameBufferTexture;
         renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0, 0.0, 0.0, 0.0);
         renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
         renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
         
         id<MTLCommandBuffer> commandBuffer = [[MetalDriver sharedDriver].defaultCommandQueue commandBuffer];
         
         id<MTLRenderCommandEncoder> renderCommandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
         
         [renderCommandEncoder setRenderPipelineState: _pipelineState];
        
         [renderCommandEncoder setVertexBuffer: _verticesBuffer
                                        offset: 0
                                       atIndex: 0];
         
         [renderCommandEncoder setVertexBuffer: _texCoordsBuffer
                                        offset: 0
                                       atIndex: 1];
         
         [renderCommandEncoder setFragmentTexture: CVMetalTextureGetTexture(texture)
                                          atIndex: 0];
         
         [renderCommandEncoder drawPrimitives: MTLPrimitiveTypeTriangleStrip
                                  vertexStart: 0
                                  vertexCount: 4
                                instanceCount: 1];
         
         [renderCommandEncoder endEncoding];
         
         [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> _Nonnull commandBuffer)
          {
              CFRelease(texture);
          }];
         
         [commandBuffer presentDrawable:drawable];
         [commandBuffer commit];
         
     }];
}


- (void)_setupMetal
{
    _metalLayer = (CAMetalLayer *)[self layer];
    
    _metalLayer.device = [MetalDriver sharedDriver].metalDevice;
    _metalLayer.pixelFormat = [MetalDriver sharedDriver].defaultMetalPixelFormat;
    
    
    [[MetalDriver sharedDriver] runSynchronouslyOnVideoProcessingQueue: ^
     {
         CVMetalTextureCacheCreate(kCFAllocatorDefault,
                                   NULL,
                                   [MetalDriver sharedDriver].metalDevice,
                                   NULL,
                                   &_texturesCache);
         
         id<MTLFunction> vertexShader = [[[MetalDriver sharedDriver] defaultShadersLibrary] newFunctionWithName:@"defaultVertexShader"];
         id<MTLFunction> fragmentShader = [[[MetalDriver sharedDriver] defaultShadersLibrary] newFunctionWithName:@"defaultTexturedFragmentShader"];
         
         MTLRenderPipelineDescriptor *renderPipelineDescriptor = [MTLRenderPipelineDescriptor new];
         renderPipelineDescriptor.vertexFunction = vertexShader;
         renderPipelineDescriptor.fragmentFunction = fragmentShader;
         
         MTLRenderPipelineColorAttachmentDescriptor *pipelineColorAttachmentDescriptor = renderPipelineDescriptor.colorAttachments[0];
         pipelineColorAttachmentDescriptor.pixelFormat = [MetalDriver sharedDriver].defaultMetalPixelFormat;
         
         _pipelineState = [[MetalDriver sharedDriver].metalDevice newRenderPipelineStateWithDescriptor:renderPipelineDescriptor error: nil];
         
         
         _verticesBuffer = [[MetalDriver sharedDriver].metalDevice newBufferWithLength: sizeof(float[16])
                                                                               options: MTLResourceCPUCacheModeDefaultCache];
         [MetalDriver fullFrameSprite: (float*)_verticesBuffer.contents];
         
         _texCoordsBuffer = [[MetalDriver sharedDriver].metalDevice newBufferWithLength: sizeof(float[8])
                                                                                options:MTLResourceCPUCacheModeDefaultCache];
         
         [MetalDriver textureCoordinatesForFullTexture: (float*)_texCoordsBuffer.contents verticallyFlipped: YES];
         
         
     }];
}


@end
